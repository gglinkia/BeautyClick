<?php
session_start();
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}
require_once("variables.php");
$conexion = mysqli_connect($host,$usuario,$contrasena,$nombre_bbdd) or die ("Error de BBDD.");

if(	!isset($_SESSION['mail'] )){
    echo"
<html>
    
    <head>
        <title>BeautyClick, tu bienestar a un click</title>
        <meta charset='UTF-80'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <link href='../css/estilo.css' rel='stylesheet'>  <!-- relación con el html:stylesheet-->
    </head>
    
    <body>  <!-- -->
        <header>  <!-- cabecera título logotipo logo... -->
            <div id='logo'>    <!-- división .. cada vez menos en uso -->
                <img src='../imagenes/logo.png' alt='logo'></a>  <!-- texto alternativo a la imagen -->

        </header>          

        </html>
        ";
	echo"No estás logineado, se te redigirá a la home en 3 segundos";
	header( "refresh:3;url=../index.html" );
	session_destroy();
    
	
}else{


echo"
<html>
    
    <head>
        <title>BeautyClick, tu bienestar a un click</title>
        <meta charset='UTF-80'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <link href='../css/estilo.css' rel='stylesheet'>  <!-- relación con el html:stylesheet-->
    </head>
    
    <body>  <!-- -->
        <header>  <!-- cabecera título logotipo logo... -->
            <div id='logo'>    <!-- división .. cada vez menos en uso -->
                <img src='../imagenes/logo.png' alt='logo'></a>  <!-- texto alternativo a la imagen -->
            </div>
             <nav class='menu'><!-- donde se delimita la propia web, los links internos de navegación -->
                   <ul><!-- unordered list, para anidar el menú... ordered seria ol en vez de ul-->
                       <!-- <li></li>  list item marca cada elemento de la lista -->
                       <!--href hipertext reference etiqueta para poner enlaces, hivervínculos  -->
                       <li><a href='../index.html'>Inicio</a></li>   <!--misma carpeta relativa  podría ser /carpeta/lkjlkj -->
                         <!-- secciones header...  -->
                      <li><a href='homeUsuarioClienteDatos.php'>Mis Datos</a>
                      <li><a href='logout.php'>Salir</a></li>
                      
                   </ul>
             </nav>
        </header>          
        <section id='perfil'>     <!--perfil personal, id perfil lo llamaré desde el css -->
            <img src='../imagenes/cliente_banner.jpg' alt='imagen usuario'>
            <h1> Menu Usuario </h1>    <!-- h1 es un formato de título.. hay h2 h3 h4 -->
            
        </section>
        </html>
        ";

	/** Botón para salir
	 */
	//echo  "<Form method='post' action='logout.php'>
	//	<input type='submit' name='Salir' value='Salir'/></form><br/><br/>";

     /** Query de datos de usuario
	 */ 
    $mail_cliente=$_SESSION['mail'];
	$consulta = "select * from usuario where mail = '$mail_cliente'";
	$resultado=mysqli_query($conexion,$consulta);
	$num_filas = mysqli_num_rows($resultado);
	if($num_filas>0){
		$fila = mysqli_fetch_array($resultado);
		extract($fila);
		$id_cliente=$idusuario;
		//echo "Hola Sr./Sra. ".$nombre." con id ".$idusuario."</br>";
		$_SESSION['id_cliente']=$id_cliente;
	}else{
		echo "Ha habido un error y se te redirigirá a la página principal";
        header( "refresh:3;url=../index.html" );
        session_destroy();
	}
	//ya tenemos el id_cliente como variable de sesión

    /** 
     * Datos de reservas
	 */
    echo " <section   id='recuadros'>  ";
	
	    /** 
        * Reservas cliente
        * Muestra lasa reservas que ha hecho el usuario cliente
	    */
        echo " <section class='recuadro'>  <!--otra secciónm inicio de sesión -->";
        //primero movemos de true a false las caducadas
        $queryCaducadas="select r.idreserva as idreservacaduc,
        r.idoferta as idoferta, r.reservado as reservado, 
        o.fecha as fecha, o.tramohorario as thorario
        from reserva as r inner join oferta as o on r.idoferta = o.idoferta 
        where o.fecha < NOW() && r.reservado = 1";

        $resultCaducadas=mysqli_query($conexion,$queryCaducadas);
        if(!empty($resultCaducadas)){
            while($filaCaduc = mysqli_fetch_array($resultCaducadas)){
				extract($filaCaduc);
                $queryUpdateCaducidad = "update reserva set reservado = 0 
                where idreserva = '$idreservacaduc'";
                //echo "hola".$idreservacaduc."hola";
                $archivadoCaduc = mysqli_query($conexion, $queryUpdateCaducidad);
                if(!empty($archivadoCaduc)){
                    echo "Archivada por caducidad la cita con fecha ".$fecha." a las ".$thorario."  </br></br>";
                }
                
            }
          
        }
       
		/** 
         * Listamos las reservas activas, las futuras
	    */
       
        $queryReservasU = "select r.idreserva as idreserva, r.reservado as reservado,
        r.idusuario as idusuarioreserva, r.idoferta as idofertausuario,
        o.idsalon as idsalonoferta, DATE_FORMAT(o.fecha, '%d/%m/%Y') as fechaservicio, 
        o.tramohorario as tramohorario, o.precio as precio, o.reservado as ofertareservado,
        so.descripcion as descripservicio, 
        s.idsalon as idsalon,
        s.nombresalon as nombresalon,s.telefono as telefonosalon, 
        s.direccion as direccionsalon, s.mail as mailsalon, 
        s.ciudad as ciudadsalon, s.cpostal as cpostalsalon
        from reserva as r
        inner join oferta as o on r.idoferta = o.idoferta 
        inner join servicio as so on o.idservicio = so.idservicio 
        inner join salon as s on s.idsalon = o.idsalon
        where r.idusuario = '$id_cliente' and r.reservado = 1";
		$resultReserva=mysqli_query($conexion,$queryReservasU);
		if(empty($resultReserva)){
			echo "No hay ninguna reserva hecha aún. </br>";
		}else{
           
			echo "<br/><b>Tus reservas:</b><br/>
				<form method='post' action='cancelarReserva.php'>
				<table border='1'>
				<tr><td>Servicio</td>
                <td>Salón</td><td>Direccion</td><td>Telefono</td>
                <td>Mail</td><td>CPostal</td>
                <td>Fecha</td><td>Hora</td><td>Precio</td>
				<td>Cancelar</td></tr>";
			while($filaR = mysqli_fetch_array($resultReserva)){
				extract($filaR);
                if(!empty($nombresalon)){
                    echo "<input type='hidden' name='idreserva' value='$idreserva' />
                    <tr><td>$descripservicio</td>              
                    <td>$nombresalon</td><td>$direccionsalon</td><td>$telefonosalon</td>
                    <td>$mailsalon</td><td>$cpostalsalon</td>
                    <td>$fechaservicio</td><td>$tramohorario</td><td>$precio</td>
                    <td colspan='5' align='right'>
                    <input class='boton' type='submit' value='cancelar'/></td></tr>
				";

                }
			
			}
			echo "
				</table></form>";
                echo " </section >  ";
		}


        /**
           * Mostramos el histórico de reservas
           *  y avisamos si ha caducado alguna y pasa a histórico
         */

        echo " <section class='recuadro'>  <!--otra secciónm inicio de sesión -->";

        	    /** 
        * Reservas cliente
        * Muestra las reservas caducadas
	    */
       
        //primero movemos de true a false las caducadas
        $queryCaducadas="select r.idreserva as idreservacaduc,
        r.idoferta as idoferta, r.reservado as reservado, 
        o.fecha as fecha, o.tramohorario as thorario
        from reserva as r inner join oferta as o on r.idoferta = o.idoferta 
        where o.fecha < NOW() and.reservado = 1 and r.idusuario = '$id_cliente'";

        $resultCaducadas=mysqli_query($conexion,$queryCaducadas);
        if(!empty($resultCaducadas)){
            while($filaCaduc = mysqli_fetch_array($resultCaducadas)){
				extract($filaCaduc);
                $queryUpdateCaducidad = "update reserva set reservado = 0 
                where idreserva = '$idreservacaduc'";
                //echo "hola".$idreservacaduc."hola";
                $archivadoCaduc = mysqli_query($conexion, $queryUpdateCaducidad);
                if(!empty($archivadoCaduc)){
                    echo "Archivada por caducidad la cita con fecha ".$fecha." a las ".$thorario."  </br></br>";
                }
                
            }
          
        }
        /**
           * Listado de histórico de reservas
           *  
         */

        $queryReservasC = "select r.idreserva as idreserva, r.reservado as reservado,
        o.idsalon as idsalon, DATE_FORMAT(o.fecha, '%d/%m/%Y') as fechaservicio, 
        o.tramohorario as tramohorario, o.precio as precio,
        so.descripcion as descripservicio,
        s.nombresalon as nombresalon,s.telefono as telefonosalon, 
        s.direccion as direccionsalon, s.mail as mailsalon, 
        s.ciudad as ciudadsalon, s.cpostal as cpostalsalon
        from reserva as r
        inner join oferta as o on r.idoferta = o.idoferta 
        inner join salon as s on o.idsalon = s.idsalon
        inner join servicio as so on o.idservicio = so.idservicio 
        where r.idusuario= '$id_cliente' and r.reservado = 0 and o.reservado = 0
        group by o.reservado";
		$resultCaduc=mysqli_query($conexion,$queryReservasC);
		if(empty($resultReserva)){
			echo "No hay ninguna reserva histórica aún. </br>";
		}else{
			echo "<br/><b>Histórico de reservas:</b><br/>
				<form method='post' action=''>
				<table border='1'>
				<tr><td>Servicio</td>
                <td>Salón</td><td>Direccion</td><td>Telefono</td>
                <td>Mail</td><td>CPostal</td>
                <td>Fecha</td><td>Hora</td><td>Precio</td>
				</tr>";
			while($filaCaduc = mysqli_fetch_array($resultCaduc)){
				extract($filaCaduc);
				echo "<tr><td>$descripservicio</td>
                
                    <td>$nombresalon</td><td>$direccionsalon</td><td>$telefonosalon</td>
                    <td>$mailsalon</td><td>$cpostalsalon</td>
                    <td>$fechaservicio</td><td>$tramohorario</td><td>$precio</td>
                    </tr>
				";
			}
			echo "
				</table></form>";
                echo " </section >  ";
		}


        /** 
         * Búsqueda de reservas
	    */
        echo " <section class='recuadrosearch'>  <!--otra secciónm inicio de sesión -->";
        echo "<br/><b>Buscar un nuevo servicio</b><br/>";
        $queryreservar = "select 
        of.idsalon as ofertaidsalon,
        s.idsalon as idsalon, s.cpostal as cpostal
        from oferta as of
        inner join salon as s on s.idsalon= of.idsalon        
        group by s.cpostal order by s.cpostal";

 

        $resultadoreservable=mysqli_query($conexion,$queryreservar);
        
        if(empty($resultadoreservable) ){
            echo "no hay ahora ninguna cita disponible para reservar";
        }else{

            while ($rowr = mysqli_fetch_assoc($resultadoreservable)){
                $datosqueryreservar[] = $rowr;
            }
            echo "<Form method='post' action='nuevaReservaPorCP.php'>";
            echo "<label for='cpostal' >Código postal: </label>
            <select name='cpostal' >";
            foreach ($datosqueryreservar as $rowr){
               echo "<option value='". $rowr['cpostal'] ."'>" . $rowr['cpostal'] ."</option>";
            }
            echo "</select></br>
            <input class='boton' type='submit' name='reservar' value='Reservar'/></form><br/><br/>";
        }
        echo " </section >  ";
        echo " </section >  ";





     echo "   <footer>   <!-- pie página -->
        <p>Alumno: Gloria Grau;  Año 2023</p>
        </footer>
        </section>
        </body>";


	mysqli_close($conexion);

} //cierro el else de verificación de id de admin

?>