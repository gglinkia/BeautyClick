<?php
session_start();
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}
require_once("variables.php");
$conexion = mysqli_connect($host,$usuario,$contrasena,$nombre_bbdd) or die ("Error de BBDD.");

if(	!isset($_SESSION['mail'] )){
	echo"no estás logineado, se te redigirá a la home en 3 segundos";
	header( "refresh:3;url=../index.html" );
	session_destroy();
	
}else{
	echo"
<html>
    
    <head>
        <title>BeautyClick, tu bienestar a un click</title>
        <meta charset='UTF-80'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <link href='../css/estilo.css' rel='stylesheet'>  <!-- relación con el html:stylesheet-->
    </head>
    
    <body>  <!-- -->
        <header>  <!-- cabecera título logotipo logo... -->
            <div id='logo'>    <!-- división .. cada vez menos en uso -->
                <img src='../imagenes/logo.png' alt='logo'></a>  <!-- texto alternativo a la imagen -->
            </div>
             <nav class='menu'><!-- donde se delimita la propia web, los links internos de navegación -->
			 <ul><!-- unordered list, para anidar el menú... ordered seria ol en vez de ul-->
             <!-- <li></li>  list item marca cada elemento de la lista -->
             <!--href hipertext reference etiqueta para poner enlaces, hivervínculos  -->
             <li><a href='../index.html'>Inicio</a></li>   <!--misma carpeta relativa  podría ser /carpeta/lkjlkj -->
               <!-- secciones header...  -->
            <li><a href='homeUsuarioSalon.php'>Mis Reservas</a>
            <li><a href='logout.php'>Salir</a></li>
            
         </ul>
             </nav>
        </header>          
        <section id='perfil'>     <!--perfil personal, id perfil lo llamaré desde el css -->
            <img src='../imagenes/salon_banner.jpg' alt='imagen usuario'>
            <h1> Menu Salon </h1>    <!-- h1 es un formato de título.. hay h2 h3 h4 -->
            
        </section>
        </html>
        ";


	
	/** Botón para salir
	 */
	//.php'>
	//<input type='submit' name='Salir' value='Salir'/></form><br/><br/>";

	/** Vinculamos el id de usuario al de salon
	 */
	$mail_gestor=$_SESSION['mail'];
	$consulta = "select idusuario from usuario where mail = '$mail_gestor'";
	$resultado=mysqli_query($conexion,$consulta);
	$num_filas = mysqli_num_rows($resultado);
	if($num_filas>0){
		$fila = mysqli_fetch_array($resultado);
		extract($fila);
		$id_gestor=$idusuario;
		//echo "idusuario   ".$idusuario."  idusuario</br>";
		$_SESSION['id_gestor']=$id_gestor;
	}else{
		echo "ha habido un error";
	}
	//ya tenemos el id_gestor que será el idsalon como variable de sesion
	

	//vamos a saber si el usuario de rol salon ya ha dado datos de su salón
	$consultaDatosSalon = "select idsalon, nombresalon, direccion, ciudad, cpostal, telefono, mail from salon where idsalon = '$id_gestor'";
	$resultadoDatosSalon=mysqli_query($conexion,$consultaDatosSalon);
	$num_filasDatosSalon = mysqli_num_rows($resultadoDatosSalon);

	

	if($num_filasDatosSalon>0){
		echo " <section   id='recuadros'>  ";

		
		echo " <section class='recuadro'> ";
		echo "<br/><b>Datos de tu salón:</b><br/>
				<form method='post' action='invalidarSalon.php'>
				<table border='1'>
				<tr><td>Nombre</td><td>Direccion</td><td>ciudad</td><td>cpostal</td><td>telefono</td><td>mail</td>
				<td>Modificar</td><td>Borrar</td></tr>";
			while($fila = mysqli_fetch_array($resultadoDatosSalon)){
				extract($fila);
				echo "<tr><td>$nombresalon</td><td>$direccion</td><td>$ciudad</td><td>$cpostal</td><td>$telefono</td><td>$mail</td>
					<td><a href='modificarSalon.php'>Modificar></a></td>
					<td><input class='boton' type='submit' name='borrar[]' value='borrar'/></td>
				</tr>";
			}
			//echo "<tr><td colspan='5' align='right'><input type='submit' value='borrar'/></td></tr>
				echo "</table></form>";
				
		echo " </section >  ";




   /** 
     ** Muestra los datos de usuario
	 */ 
    $mail_cliente=$_SESSION['mail'];
	$consultapersona = "select * from usuario where mail = '$mail_cliente'";
	$resultadopersona=mysqli_query($conexion,$consultapersona);
	$num_filas = mysqli_num_rows($resultadopersona);
	if($num_filas>0){
		$filapersona = mysqli_fetch_array($resultadopersona);
		extract($filapersona);
		$id_cliente=$idusuario;
		//echo "Hola Sr./Sra. ".$nombre." con id ".$idusuario."</br>";
		$_SESSION['id_cliente']=$id_cliente;
	}else{
		echo "Ha habido un error y se te redirigirá a la página principal";
        header( "refresh:3;url=../index.html" );
        session_destroy();
	}
	//ya tenemos el id_cliente como variable de sesión
    echo "  <section class='recuadro'>  
    <br/><b>Tus datos personales:</b><br/>
    <form method='post' action='invalidarUsuarioSalon.php'>
    <table border='1'>
    <tr><td>Nombre</td><td>Apellidos</td><td>Teléfono</td><td>Mail</td>
    <td>Ciudad</td><td>Código Postal</td>
    <td>Modificar</td><td>Borrar</td></tr>";
    $resultadopersona=mysqli_query($conexion,$consultapersona);
	$num_filas = mysqli_num_rows($resultadopersona);
    while($fila = mysqli_fetch_array($resultadopersona)){
        extract($fila);
        echo "<tr><td>$nombre</td><td>$apellidos</td><td>$telefono</td><td>$mail</td>
        <td>$ciudad</td><td>$cpostal</td>
        <td><a href='modificarUsuarioRolSalon.php?idcliente=$id_cliente'>Modificar></a></td>
        <td><input class='boton' type='submit' name='borrar[]' value='borrar' /></td>
        </tr>";
    }
//echo "<tr><td colspan='5' align='right'><input type='submit' value='borrar'/></td></tr>
    echo "</table></form>";
		
    echo " </section >  ";


	echo " </section >  ";



	
				
			
	}else{

		/**
		 * Datos iniciales, completar
		 * Si al inicio de sesión de usuario salón vemos
		 * que aún no hay datos del salón que gestiona, le pedimos que los rellene.
		 * Si ya hay datos, no se mostrará este formulario
		 */
		
        
                  
        
		echo "	<section class='recuadro'>
		Completa los datos iniciales de tu salón";
	
		echo "	Formulario de datos de Salon:<br/>
		<Form method='post' action='nuevoSalon.php'><br/>
		<label for='nombresalon' >Nombre: </label>
		<input type='text' value='nombresalon' name='nombresalon' required='required'></br>
		<label for='direccion' >Dirección: </label>
		<input type='text' value='direccion' name='direccion' required='required'></br>
        <label for='ciudad' >Ciudad: </label>
		<input type='text' value='ciudad' name='ciudad' required='required'></br>
        <label for='cpostal' >CPostal: </label>
		<input type='text' value='cpostal' name='cpostal' required='required'></br>
        <label for='telefono' >Teléfono: </label>
		<input type='text' value='telefono' name='telefono'  required='required'></br>
        <label for='mail' >Email: </label>
		<input type='email' value='mail' name='mail'  required='required'></br></br>
		<input class='boton' type='submit' name='crearSalon' value='Crear Salon'/></form><br/></br>
		</section >  ";
	
	}




	echo "   <footer>   <!-- pie página -->
	<p>Alumno: Gloria Grau;  Año 2023</p>
	</footer>
	</body>";	

	mysqli_close($conexion);

} //cierro el else de verificación de id de admin

?>