<?php
session_start();
if (ini_get('register_globals'))
{
    foreach ($_SESSION as $key=>$value)
    {
        if (isset($GLOBALS[$key]))
            unset($GLOBALS[$key]);
    }
}
require_once("variables.php");
$conexion = mysqli_connect($host,$usuario,$contrasena,$nombre_bbdd) or die ("Error de BBDD.");

if(	!isset($_SESSION['mail'] )){
    echo"
<html>
    
    <head>
        <title>BeautyClick, tu bienestar a un click</title>
        <meta charset='UTF-80'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <link href='../css/estilo.css' rel='stylesheet'>  <!-- relación con el html:stylesheet-->
    </head>
    
    <body>  <!-- -->
        <header>  <!-- cabecera título logotipo logo... -->
            <div id='logo'>    <!-- división .. cada vez menos en uso -->
                <img src='../imagenes/logo.png' alt='logo'></a>  <!-- texto alternativo a la imagen -->

        </header>          

        </html>
        ";
	echo"No estás logineado, se te redigirá a la home en 3 segundos";
	header( "refresh:3;url=../index.html" );
	session_destroy();
    
	
}else{
	echo"
<html>
    
    <head>
        <title>BeautyClick, tu bienestar a un click</title>
        <meta charset='UTF-80'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <link href='../css/estilo.css' rel='stylesheet'>  <!-- relación con el html:stylesheet-->
    </head>
    
    <body>  <!-- -->
        <header>  <!-- cabecera título logotipo logo... -->
            <div id='logo'>    <!-- división .. cada vez menos en uso -->
                <img src='../imagenes/logo.png' alt='logo'></a>  <!-- texto alternativo a la imagen -->
            </div>
             <nav class='menu'><!-- donde se delimita la propia web, los links internos de navegación -->
			 <ul><!-- unordered list, para anidar el menú... ordered seria ol en vez de ul-->
				<!-- <li></li>  list item marca cada elemento de la lista -->
				<!--href hipertext reference etiqueta para poner enlaces, hivervínculos  -->
				<li><a href='../index.html'>Inicio</a></li>   <!--misma carpeta relativa  podría ser /carpeta/lkjlkj -->
				<!-- secciones header...  -->
				<li><a href='homeUsuarioSalonDatos.php'>Mis Datos</a>
				<li><a href='logout.php'>Salir</a></li>
			
		 </ul>
             </nav>
        </header>          
        <section id='perfil'>     <!--perfil personal, id perfil lo llamaré desde el css -->
            <img src='../imagenes/salon_banner.jpg' alt='imagen usuario'>
            <h1> Menu Salon </h1>    <!-- h1 es un formato de título.. hay h2 h3 h4 -->
            
        </section>
        </html>
        ";


	
		

	/** Vinculamos el id de usuario al de salon
	 */
	$mail_gestor=$_SESSION['mail'];
	$consulta = "select idusuario from usuario where mail = '$mail_gestor'";
	$resultado=mysqli_query($conexion,$consulta);
	$num_filas = mysqli_num_rows($resultado);
	if($num_filas>0){
		$fila = mysqli_fetch_array($resultado);
		extract($fila);
		$id_gestor=$idusuario;
		//echo "idusuario   ".$idusuario."  idusuario</br>";
		$_SESSION['id_gestor']=$id_gestor;
	}else{
		echo "ha habido un error";
	}
	//ya tenemos el id_gestor que será el idsalon como variable de sesion
	

	//vamos a saber si el usuario de rol salon ya ha dado datos de su salón
	$consultaDatosSalon = "select idsalon, nombresalon, direccion, ciudad, cpostal, telefono, mail from salon where idsalon = '$id_gestor'";
	$resultadoDatosSalon=mysqli_query($conexion,$consultaDatosSalon);
	$num_filasDatosSalon = mysqli_num_rows($resultadoDatosSalon);

	if($num_filasDatosSalon>0){

		/** 
     * Datos de servicios ofertados
	 */
		
		echo " <section   id='recuadros'>  ";
		echo " <section class='recuadro'> ";	
				$queryservicios = "select t.nombreservicio as nombretiposervicio, t.descripcion as tiposervicio,
				s.descripcion as descripcionservicio, DATE_FORMAT(o.fecha, '%d/%m/%Y') as fechaservicio, o.tramohorario as tramohorario, 
				o.precio as precio, o.idoferta as idofertado
				from tiposervicio as t
				inner join servicio as s on s.idtipo = t.idtipo 
				inner join oferta as o on s.idservicio = o.idservicio 
				where o.idsalon= '$id_gestor' and reservado = '0'";
				$resultadoServicios=mysqli_query($conexion,$queryservicios);
				$num_filas = mysqli_num_rows($resultadoServicios);
				if(mysqli_num_rows($resultadoServicios) == 0){
					echo "No hay servicios ofertados";
				}else{
					
						
						echo "<br/><b>Tus servicios ofertados:</b><br/>
						<form method='post' action=''>
						<table border='1'>
						<tr><td>Tipo</td><td>Descripcion</td><td>Servicio</td>
						<td>Fecha</td><td>Horario</td><td>Precio</td>
						<td>Id</td><td>id</td></tr>";
					while($fila = mysqli_fetch_array($resultadoServicios)){
						extract($fila);
					echo "<tr><td>$nombretiposervicio</td><td>$tiposervicio</td>
						<td>$descripcionservicio</td><td>$fechaservicio</td>
						<td>$tramohorario</td><td>$precio</td><td>$idofertado</td>
						<input type='hidden' name='idofertado' value='$idofertado' />
						<td><a href='invalidarOferta.php?idofertado=$idofertado'>Invalidar</a></td>
					</tr>";
					}
					echo "</table></form>";

				}
				
				echo " </section > ";

			
				/**
				 * reservas recibidas, vista
				 * el usuario salon puede ver las reservas recibidas
				 * 
				 */
				$queryrecibidas = "select r.idusuario as idusuarioreservado, 
				r.idoferta as idofertareservado, of.idservicio as ofertaidservicio, 
				of.fecha as fecha, of.tramohorario as tramohorario, 
				of.precio as precio, of.reservado as reservadosi, of.idsalon as ofidsalon,
				us.nombre as nombrecliente, us.telefono as telefonocliente, 
				us.mail as mailcliente, so.descripcion as servicio 
				from reserva as r inner join oferta as of on r.idoferta = of.idoferta 
				inner join usuario as us on us.idusuario = r.idusuario 
				inner join servicio as so on so.idservicio = of.idservicio 
				where  r.reservado = 1 and of.reservado = 1 and of.idsalon = '$id_gestor'";
				$resultadoRecibidas=mysqli_query($conexion,$queryrecibidas);
				echo " <section class='recuadro'> ";
				if(empty($resultadoRecibidas)){
					echo "No has aún recibido reservas";
				}else{
					
					echo "<br/><b> </br> </br>Reservas recibidas:</b><br/>
					<form method='post' action=''>
					<table border='1'>
					<tr><td>Cliente</td><td>Telefono</td><td>Email</td>
					<td>Servicio</td><td>Fecha</td><td>Horario</td><td>Precio</td>
					</tr>";
					while($filaRecibidas = mysqli_fetch_array($resultadoRecibidas)){
						extract($filaRecibidas);
						echo "<tr><td>$nombrecliente</td><td>$telefonocliente</td>
						<td>$mailcliente</td><td>$servicio</td><td>$fecha</td>
						<td>$tramohorario</td><td>$precio</td>
						
						</tr>";
					}
					echo "</table></form>";		
					
				}
				echo " </section >   ";

			
		
				/**
				 * oferta de servicio, alta
				 * el usuario salón puede crear una oferta de servicio
				 * indicando horario que ofrece y precios
				 */

				echo " <section class='recuadro'> ";
				echo "<br/><b>Publicación de oferta de servicio:<br/>";
				$queryserviciosytipos = "select t.nombreservicio as nombretiposervicio, 
				t.descripcion as descriptiposervicio, s.descripcion as descripcionservicio,
				s.idservicio as idservicio
				from tiposervicio as t
				inner join servicio as s on s.idtipo = t.idtipo group by descripcionservicio order by nombretiposervicio";
				$resultadotipos=mysqli_query($conexion,$queryserviciosytipos);
				
				if(mysqli_num_rows($resultadotipos) == 0){
					echo "No hay tipos de servicios";
				}else{

					while ($row = mysqli_fetch_assoc($resultadotipos)){
    					$datosquery[] = $row;
					}
					echo "<Form method='post' action='nuevaOferta.php'>";
					//echo "Tipo de servicio:
					//<select name='nombretiposervicio'>";
					//foreach ($datosquery as $row){
					//	echo "<option value='" . $row['nombretiposervicio'] ."'>" . $row['nombretiposervicio'].":   ".$row['descriptiposervicio'] ."</option>";
					//}
					echo "</select></br>
					Servicio:
					<select name='idservicio'>";
					foreach ($datosquery as $row){
						echo "<option value='". $row['idservicio'] ."'>" . $row['nombretiposervicio'] ."-->   " . $row['descripcionservicio'] ."</option>";
					};
					echo "</select ></br>
					<label for='fecha' >Fecha: </label>
					<input type='date' name='fecha' 	data-date='' data-date-format='DD MMMM YYYY' value='2015-08-09'></br>
					<label for='horario' >Tramo horario: </label>
					<input type='text' value='horario' name='tramohorario'  required='required'></br>
					<label for='precio' >Precio: </label>
					<input type='text' value='precio' name='precio'  required='required'></br>
					<input class='boton' type='submit' name='nuevoServicio' value='Agregar'/></form></br><br/><br/>";
				}
				echo " </section > </br></br><br/><br/>  ";

				
				echo"<br/></br></br></br><h1>Aportación de datos</h1>";



				
				/**
				 *Alta de nuevo servicio
				 */

			
				$querytipos = "select * from tiposervicio";
				$resultadotipo=mysqli_query($conexion,$querytipos);
				$num_filast = mysqli_num_rows($resultadotipo);
				if($num_filast=0){
					echo "No hay registros.";
		
				}else{		
					echo " <section class='recuadro'> ";
					echo "<br/><b>Alta de un nuevo servicio:</b><br/>
					
					<table border='1'>
					<tr><td>Tipo de Servicio</td><td>Descripción del tipo de servicio</td>
					<td>Nuevo servicio</td></tr>";
					while($filatipos = mysqli_fetch_array($resultadotipo)){
						extract($filatipos);
						echo "<tr><td>$nombreservicio</td><td>$descripcion</td>
						<td><a href='nuevoServicio.php?idtipo=$idtipo'>Crear></a></td></tr>
						";
					}
					echo "</table>";	
					
					echo "<br/><b>Formulario de alta de nuevo tipo de servicio:</b><br/>
					<Form method='post' action='nuevoTipoServicio.php'>
					<label for='tiposervicio' >Nombre del tipo de servicio: </label>
					<input type='text' value='' name='tiposervicio' required='required'><br/>
					<label for='descripcion' >Descripción: </label>
					<input type='text' value='' name='descripcion' required='required'></br>
					<input class='boton' type='submit' name='nuevoServicio' value='Crear Servicio'/></form><br/><br/>";
					echo " </section >  ";
					
				}
				
				
				echo " </section >  ";
				
			
	}else{

	

		/**
		 * Datos iniciales, completar
		 * Si al inicio de sesión de usuario salón vemos
		 * que aún no hay datos del salón que gestiona, le pedimos que los rellene.
		 * Si ya hay datos, no se mostrará este formulario
		 */
		
        
                  
        
		echo "
		<section class='recuadro'>
		Completa los datos iniciales de tu salón";
	
		echo "	Formulario de datos de Salon:<br/>
		<Form method='post' action='nuevoSalon.php'><br/>
		<label for='nombresalon' >Nombre: </label>
		<input type='text' value='nombresalon' name='nombresalon' required='required'></br>
		<label for='direccion' >Dirección: </label>
		<input type='text' value='direccion' name='direccion' required='required'></br>
        <label for='ciudad' >Ciudad: </label>
		<input type='text' value='ciudad' name='ciudad' required='required'></br>
        <label for='cpostal' >CPostal: </label>
		<input type='text' value='cpostal' name='cpostal' required='required'></br>
        <label for='telefono' >Teléfono: </label>
		<input type='text' value='telefono' name='telefono'  required='required'></br>
        <label for='mail' >Email: </label>
		<input type='email' value='mail' name='mail'  required='required'></br></br>
		<input class='boton' type='submit' name='crearSalon' value='Crear Salon'/></form><br/></br>
		 </section >  ";
		 
	}


	

	echo "   <footer>   <!-- pie página -->
	<p>Alumno: Gloria Grau;  Año 2023</p>
	</footer>
	</body>";	

	mysqli_close($conexion);

} //cierro el else de verificación de id de admin

?>