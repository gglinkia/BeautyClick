CREATE DATABASE IF NOT EXISTS beautyclick;

CREATE TABLE IF NOT EXISTS `usuario` (
  `idusuario` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `contrasena` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `cpostal` varchar(255) DEFAULT NULL,
  `rol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
) ;

CREATE TABLE IF NOT EXISTS `salon` (
  `idsalon` int NOT NULL,
  `nombresalon` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `cpostal` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idsalon`),
  FOREIGN KEY (`idsalon`) REFERENCES `usuario` (`idusuario`)
) ;

CREATE TABLE IF NOT EXISTS `tiposervicio` (
  `idtipo` int NOT NULL AUTO_INCREMENT,
  `nombreservicio` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idtipo`)
) ;

CREATE TABLE IF NOT EXISTS `servicio` (
  `idservicio` int NOT NULL AUTO_INCREMENT,
  `idtipo` int NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idservicio`),
  FOREIGN KEY (`idtipo`) REFERENCES `tiposervicio` (`idtipo`)
) ;


CREATE TABLE IF NOT EXISTS `oferta` (
  `idoferta` int NOT NULL AUTO_INCREMENT,
  `idsalon` int NOT NULL,
  `idservicio` int NOT NULL,
  `fecha` date DEFAULT NULL,
  `tramohorario` varchar(255) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `reservado` boolean default FALSE,
  PRIMARY KEY (`idoferta`),
  FOREIGN KEY (`idsalon`) REFERENCES `salon` (`idsalon`),
  FOREIGN KEY (`idservicio`) REFERENCES `servicio` (`idservicio`)
) ;

CREATE TABLE IF NOT EXISTS `reserva` (
  `idreserva` int NOT NULL AUTO_INCREMENT,
  `idusuario` int NOT NULL,
  `idoferta` int NOT NULL,
  `reservado` boolean default TRUE,
  PRIMARY KEY (`idreserva`),
  FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`),
  FOREIGN KEY (`idoferta`) REFERENCES `oferta` (`idoferta`)
) ;






